voiceLyrics = \lyricsto "voice" {

  Let Beau -- ty a -- wake
  in the morn from beau -- ti -- ful dreams,
  Beau -- ty a -- wake from rest!
  Let Beau -- ty a -- wake
  For Beau -- ty's sake
  In the hour when the birds a -- wake in the brake
  And the stars are bright in the west!

  Let Beau -- ty a -- wake
  in the eve from the slum -- ber of day,
  A -- wake in the crim -- son eve!
  In the day's dusk end
  When the shades as -- cend
  Let her wake to the kiss of a ten -- der friend
  To ren -- der again and re -- ceive!

}
