\version "2.20.0"

pos =
#(define-event-function
  (parser location position)
  (markup?)
  #{
    \markup \small \bold #position
  #}
)

posV = \markup \small \bold "V"
posII = \markup \small \bold "II"
posIII = \markup \small \bold "III"
posIV = \markup \small \bold "IV"
posVII = \markup \small \bold "VII"

ottavaStart = {
  \set Staff.ottavation = #"8vb"
  \once \override Staff.OttavaBracket.direction = #DOWN
  \set Voice.middleCPosition = #1
}

ottavaStop = {
  \unset Staff.ottavation
  \unset Voice.middleCPosition
}

guitarDynamics = {

  \barNumberCheck #1
  s4^\markup \italic { "poco" \dynamic f } s2. |

}

guitarBass = \relative c' {

  \barNumberCheck #1
  a2. a4. |

  \barNumberCheck #2 % Let Beauty awake
  a2. a4. |
  g2. g4. |
  fis4. e c4 d8 |
  g4. fis e |

}

guitarArpeggio = \relative c' {

  \barNumberCheck #1
  a32^\posV c e c e a c( a) e c e c \repeat unfold 2 { a32 c e c e a c( a) e c e c } |

  \barNumberCheck #2 % Let Beauty awake
  \repeat unfold 3 { a32 c e c e a c( a) e c e c } |
  \repeat unfold 3 { g32 c e c e a c( a) e c e c } |
  fis,32^\posVII a dis a dis fis b fis dis a dis a
  e b' e b e g b g e b e b
  c, e g c c e g b <a f c d, a>8 |

}

guitar = {
  \clef "treble_8"
  \mergeDifferentlyHeadedOn
  \mergeDifferentlyDottedOn
  \common
  \set fingeringOrientations = #'(left)

  <<
    \voiceTwo \guitarArpeggio \\
    \voiceOne \guitarBass
  >>
}

% Local Variables:
% outline-regexp: "[a-zA-Z]+? +?="
% End:
