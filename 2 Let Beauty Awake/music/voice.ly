\version "2.18.2"

voiceMusic = \relative c' {
  \set Staff.instrumentName = "Voice"
  \common
  \clef "treble_8"

  \barNumberCheck #1
  R4.*3 |

  \barNumberCheck #2 % Let Beauty awake
  r8 r a\(  a a8. b16 c4.~ |
  c8 b a  c4 c8  c8. b16 a8 |
  b4. \) b8.\( c16 d8  b4 a8 |
  g4.\)  r8 r b\(  b g' fis |
  e4.~  e4\) b8\(  e( d) b |
  d4.~  d8\)

}
