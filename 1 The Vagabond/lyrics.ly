voiceLyrics = \lyricsto "voice" {

  Give to me the life I love,
  Let the lave go by me,
  Give the jol -- ly heaven a -- bove,
  And the by -- way nigh me.
  Bed in the bush with stars to see,
  Bread I dip in the ri -- ver_
  There's the life for a man like me,
  There's the life for ev -- er.

  Let the blow fall soon or late,
  Let what will be o'er me;
  Give the face of earth a -- round,
  And the road be -- fore me.
  Wealth I seek not, hope nor love,
  Nor a friend to know me;
  All I seek, the heaven a -- bove,
  And the road be -- low me.

  Or let au -- tumn fall on me
  Where a -- field I lin -- ger.
  Si -- lenc -- ing the bird on tree,
  Bi -- ting the blue fin -- ger.
  White as meal the fros -- ty field—
  Warm the fire -- side ha -- ven—
  Not to au -- tumn will I yield,
  Not to win -- ter e -- ven!

}
