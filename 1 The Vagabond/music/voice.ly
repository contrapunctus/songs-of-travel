\version "2.18.2"

voiceMusic = \relative c' {
  \set Staff.instrumentName = "Voice"
  \common

  \barNumberCheck #1
  R1*6 |

  \barNumberCheck #7 %% Give to me the life I love,
  \tuplet 3/2 {e4^\markup \italic "risoluto." g8} \tuplet 3/2 {b4 b8} b4 d |
  b2 b8-. a-. g-. g-. |
  b2 e,4 r |

  \barNumberCheck #10 %% Give the jolly heaven above,
  \tuplet 3/2 {g4 bes8} \tuplet 3/2 {d4 d8} d4 f |
  d2 d8-. c-. bes-. c-. |
  d2 d4 r |

  \barNumberCheck #13 %% Bed in the bush with stars to see,
  d4 d8 c d4 e |
  f4. c8 aes4 r |

  \barNumberCheck #15 %% Bread I dip in the river
  c4.\( bes8 c4 d8 d |
        ees1\< |

  \barNumberCheck #17 %% There's the life for a man like me
  d4\) r\! g4.\f g8 |
  f4 bes,8 bes c4 d |
  g,1~\> |

  \barNumberCheck #20 %% There's the life forever.
  g4\! r e4. g8 |
  b2.\< d4\(\> |
  e,2\)\! e4 r |

  \barNumberCheck #23
  R1*4 |

  \barNumberCheck #27 %% Let the blow fall soon or late,
  \tuplet 3/2 {e4^\markup \italic "risoluto." g8} \tuplet 3/2 {b4 b8} b4 d |
  b2 b8-. a-. g-. g-. |
  b2 e,4 r |

  \barNumberCheck #30 %% Give the face of earth around,
  \tuplet 3/2 {g4 bes8} \tuplet 3/2 {d4 d8} d4 f |
  d2 d8-. c-. bes-. c-. |
  d2 d4 r |

  \barNumberCheck #33 %% Wealth I seek not,
  d4. c8 d4 e |
  f4. c8 aes4 r |

  \barNumberCheck #35 %% Nor a friend to know me;
  c4\( c8( bes) c4 d |
  ees1\< |

  \barNumberCheck #37 %% All I seek, the heaven above,
  d4\) r\! g4--\f g-- |
  f4. bes,8 c4 d |
  g,1~\> |

  \barNumberCheck #40 %% And the road below me.
  g4\! r e4.\p g8 |
  b2.\< d4\(\> |
  e,2\)\! e4 r |
  R1 |
  \bar "||"

  \barNumberCheck #44 % Or let autumn fall on me
  \key gis \minor
  r2 gis4.^\markup { \dynamic mf \italic "robustamente."} ais8 |
  b4 gis ais fis |
  gis2 gis8-. ais-. b-. ais-. |
  gis2( fis) |
  dis r |

  \barNumberCheck #49 % Silencing the bird on tree
  b'4. cis8 d4 b |
  cis4 a fis2 |
  b8\<( cis) d e\! fis2^\markup \italic { "poco" \dynamic f } |
  eis2 cis |

  \barNumberCheck #53 % White as meal the frosty field
  d4.^\markup \italic { "meno" \dynamic f } d8 c4 f, |
  g\( a bes2\) |
  bes4.\( bes8 aes4 des, |
  ees2 f\) |

  \barNumberCheck #55 % Not to autumn will I yield
  r cis'4. dis8 |
  e4 dis cis b |
  gis2 e'4.\< fis8\! | \bar "||"
  \key e \minor
  g2. fis4 |
  e4 b r2 |
  R1*2 |

}
