\version "2.20.0"

pos =
#(define-event-function
  (parser location position)
  (markup?)
  #{
    \markup \small \bold #position
  #}
)

posI = \markup \small \bold "I"
posII = \markup \small \bold "II"
posIII = \markup \small \bold "III"
posIV = \markup \small \bold "IV"
posVII = \markup \small \bold "VII"

ottavaStart = {
  \set Staff.ottavation = #"8vb"
  \once \override Staff.OttavaBracket.direction = #DOWN
  \set Voice.middleCPosition = #1
}

ottavaStop = {
  \unset Staff.ottavation
  \unset Voice.middleCPosition
}

doubtOne = \markup \column \italic {
  \line { "Do we really want such a strong" }
  \line { "emphasis on the F minor?" }
}

guitarDynamics = {

  \barNumberCheck #1
  s4^\markup \concat { \dynamic p " ma sempre marcato."} s2. |
  s1*2 |
  s4\< s\! s\> s\! |
  s1*2 |

  \barNumberCheck #7 % Give to me the life I love
  s1*3 |

  \barNumberCheck #10 % Give the jolly heaven above
  s1*3 |

  \barNumberCheck #13 % Bed in the bush with stars to see
  s1*2 |

  \barNumberCheck #15 % Bread I dip in the river
  s1 |
  s4\< s2. |

  \barNumberCheck #17 % There's the life for a man like me
  s4\! s s2\f |
  s1 |
  s4\> s2 s4\! |

  \barNumberCheck #20 % There's the life for ever.
  s4\pp s2. |
  s4^"colla voce." s2. |
  s4\pp s2. |

}

introPhraseLowLH = \relative c {

  <e,-0 b'-1>4-.^\posII <fis b>-. <e b'>-. <fis b>-. |
  <e b'>4-. <fis b>-. <e b'>-. <fis b>-. |
  \repeat unfold 2 { <e b' e>4-. <fis b>-. <e b'>-. <fis b>-. | }

}

leftHand = \relative c {
  \set fingeringOrientations = #'(left)

  \barNumberCheck #1
  <<
    { <b e>4-._\markup \italic {"sempre pesante il basso."} ^\posVII <b fis'>-. <b e>4-. <b fis'>-. | }

    \new Staff \with {
      \remove "Time_signature_engraver"
      alignAboveContext = "main"
      \magnifyStaff #2/3
      firstClef = ##f
      \clef "treble_8"
    }
    \relative c, {
      %% FIXME - either the sharp sign is displayed or (if I add a
      %% \key) a key signature is displayed
      %% \key e \minor
      <e-0 b'-1>4-.^\posII^"orig." <fis-1 b-1>-. <e b'>-. <fis b>-. |
    }
  >>
  <b e>4-. <b fis'>-. <b e>4-. <b fis'>-. |
  \introPhraseLowLH

  \barNumberCheck #7 % Give to me the life I love
  \repeat unfold 2 { <e, b' e>4-. <fis b>-. <e b'>-. <fis b>-. | }
  <e b'-2>4-. <fis b>-. <e b'>-. <fis b>-. |

  \barNumberCheck #10 % Give the jolly heaven above
  <g d'>-. <a d>-. <g d'>-. <a d>-. |
  <g d'>-. f'-. ees-. f-. |
  bes,-. d-. bes-. d-. |

  \barNumberCheck #13 % Bed in the bush with stars to see
  g,4-. bes-. g-. c-. |
  <f, c'-2 f-3>-.^\doubtOne c'-. f,-. g-. |

  \barNumberCheck #15 % Bread I dip in the river
  aes-. c-. aes-. bes-. |
  ees ges ees c |

  \barNumberCheck #17 % There's the life for a man like me
  d c <bes d> <g d> |
  a-0 g-3 a bes |
  c e c e |

  \barNumberCheck #20 % There's the life for ever.
  <b e>4-.^\posVII <b fis'>-. <b e>4-. <b fis'>-. |
  <b e>4-. <b fis'>-. <b e>4-. <b fis'>-. |
  <e,-0 b'-1>4-. <fis b>-. <e b'>-. <fis b>-. |

  \barNumberCheck #23
  \introPhraseLowLH

  \barNumberCheck #27 %% Let the blow fall soon or late,
  \repeat unfold 2 { <e b' e>4-. <fis b>-. <e b'>-. <fis b>-. | }
  <e b'-2>4-. <fis b>-. <e b'>-. <fis b>-. |

  \barNumberCheck #30 %% Give the face of earth around,
  <g d'>-. <a d>-. <g d'>-. <a d>-. |
  <g d'>-. f'-. ees-. f-. |
  bes,-. d-. bes-. d-. |

  \barNumberCheck #33 %% Wealth I seek not,
  g,4-. bes-. g-. c-. |
  <f, c'-2 f-3>-.^\doubtOne c'-. f,-. g-. |

  \barNumberCheck #35 %% Nor a friend to know me;
  aes-. c-. aes-. bes-. |
  ees ges ees c |

  \barNumberCheck #37 %% All I seek, the heaven above,
  d c <bes d> <g d'> |
  a-0 g-3 a bes |
  c e c e |

  \barNumberCheck #40 %% And the road below me.
  <b e>4-.^\posVII <b fis'>-. <b e>4-. <b fis'>-. |
  <b e>4-. <b fis'>-. <b e>4-. <b fis'>-. |
  <e,-0 b'-1>4-. <fis b>-. <e b'>-. <fis b>-. |
  <e-0 b'-1>4-. <fis b>-. <e b'>-. <fis b>-. |
  \bar "||"

  \barNumberCheck #44 % Or let autumn fall on me
  \key gis \minor
  gis4 ais gis ais |
  gis' dis fis dis |
  gis,8-. ais-. b-. dis-.  gis-. ais-. b-. ais-. |
  << \relative c' {
    \voiceOne
    gis2 fis |
    dis2 r |
  } \\
  \relative c {
    \voiceTwo
    r4 gis-- r ais-- |
    r4 gis8-. ais-.  bis-. dis-. gis-. ais-. |
  } >>

  \barNumberCheck #49 % Silencing the bird on tree
  \voiceTwo
  b,4 cis b fis |
  a fis b8-. cis-. d-. fis-. |

  \barNumberCheck #51 % Biting the blue finger
  << \relative c {
    \voiceOne b cis d e  fis2 |
    eis2 cis |
  } \\
     \relative c {
       \voiceTwo s2 r4 b-- |
       r4 cis-- r fis,-- |
     } >>
}

introPhraseLowRH = \relative c {

  r2 \tuplet 3/2 { e4-. g8-. } \tuplet 3/2 { b4-. b8-. } |
  b2.\( <d a fis>4 \) |
  <e b g>1--^\posI |
  q-- |

}

rightHand = \relative c' {
  \set fingeringOrientations = #'(left)

  \barNumberCheck #1
  r2 \tuplet 3/2 { e4-. g8-. } \tuplet 3/2 { b4-. b8-. } |
  b2 r |
  \introPhraseLowRH

  \barNumberCheck #7 % Give to me the life I love
  q2 <b, g e>4 <d a fis>^\posII |
  <b g e>2 <g' b,>8-.^\posII <fis a,>-. <e g,>-. <fis a,>-. |
  \break
  <b, g e-3>1 |

  \barNumberCheck #10 % Give the jolly heaven above
  <bes g>2 <d bes g>4 <f c a> |
  <d bes g>2 <d bes>8 <c a> bes <c a> |
  <d bes f>1^\posIII |
  \break

  \barNumberCheck #13 % Bed in the bush with stars to see
  <g bes,>4-. q8-. <g a,>-. <g bes,>4 <g bes,> |
  <f c aes>4. \startBarre "I" c8 <aes f_->4 \stopBarre <bes des> |

  \barNumberCheck #15 % Bread I dip in the river
  <c ees>4 \startBarre "III" <c aes'>8 <bes g'> \stopBarre <c aes'>4 \startBarre "IV" <d f aes> \stopBarre |
  \break
  <bes ees ges bes>1 |

  \barNumberCheck #17 % There's the life for a man like me
  <fis a d>2 <g bes d g>4. q8 |
  <f-4 a c f>4 <f a> <ees-2 g c-3> <fis d'> |
  <\parenthesize e g c e>1 |

  \barNumberCheck #20 % There's the life for ever.
  \relative c' {
    \tuplet 3/2 { e4-. g8-. } \tuplet 3/2 { b4-. b8-. } b2~ |
    b2. <d g,>4 |
    <e, b g>1--^\posI |
  }

  \barNumberCheck #23
  \introPhraseLowRH

  \barNumberCheck #27 %% Let the blow fall soon or late,
  q2 <b' g e>4 <d a fis>^\posII |
  <b g e>2 <g' b,>8-.^\posII <fis a,>-. <e g,>-. <fis a,>-. |
  \break
  <b, g e-3>1 |

  \barNumberCheck #30 %% Give the face of earth around,
  <bes g>2 <d bes g>4 <f c a> |
  <d bes g>2 <d bes>8 <c a> bes <c a> |
  <d bes f>1^\posIII |
  \break

  \barNumberCheck #33 %% Wealth I seek not,
  <g bes,>4-. q8-. <g a,>-. <g bes,>4 <g bes,> |
  <f c aes>4. \startBarre "I" c8 <aes f_->4 \stopBarre <bes des> |

  \barNumberCheck #35 %% Nor a friend to know me;
  <c ees>4 \startBarre "III" <c aes'>8 <bes g'> \stopBarre <c aes'>4 \startBarre "IV" <d f aes> \stopBarre |
  \break
  <bes ees ges bes>1 |

  \barNumberCheck #37 %% All I seek, the heaven above,
  <fis a d>2 <g bes d g>4 q |
  <f-4 a c f>4. <f a>8 <ees-2 g c-3>4 <fis d'> |
  <\parenthesize e g c e>1 |

  \barNumberCheck #40 %% And the road below me.
  \relative c' {
    \tuplet 3/2 { e4-. g8-. } \tuplet 3/2 { b4-. b8-. } b2~ |
    b2. <d g,>4 |
    <e, b g>1--^\posI |
    <e b g>2.\<-- fis4\! |
  }
  \bar "||"

  \barNumberCheck #44 % Or let autumn fall on me
  \key gis \minor
  <gis' dis b>2 <dis b>4. q8 |
  q4 q q q |
  <gis dis b>2 <dis b> |
  \voiceThree
  r4 <dis b>-- r <dis ais>-- |
  r <dis b>8 r r2 |

  \barNumberCheck #49 % Silencing the bird on tree
  <d fis>4. q8 q4 q |
  q q q2 |

  \barNumberCheck #51 % Biting the blue finger
  << \relative c' {
    \voiceOne b8 cis d e fis2 |
    eis2 cis |
  } \\ \relative c' {
    s2 r4 <b d> |
    r4 <gis cis> r <fis ais> |
  } >>

}

guitar = {
  \clef "treble_8"
  % \mergeDifferentlyHeadedOn
  \common

  <<
    \voiceTwo \leftHand \\
    \voiceOne \rightHand
  >>

}

% Local Variables:
% outline-regexp: "[a-zA-Z]+? +?="
% End:
