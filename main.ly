\version "2.18.2"

\include "lsr952.ly"
\include "project-common.ly"

\include "1 The Vagabond/common.ly"
\include "1 The Vagabond/music/voice.ly"
\include "1 The Vagabond/lyrics.ly"
\include "1 The Vagabond/music/guitar.ly"

\header {
  title    = \markup \fontsize #2 "Songs of Travel"
  composer = "Ralph Vaughan Williams"
  poet = "Robert Louis Stevenson"
  arranger = "arr. Kashish"
  copyright = \copyright
}

\score {
  <<
    \new Staff << \new Voice = "voice" { \voiceMusic } >>
    \new Lyrics \voiceLyrics
    \new Dynamics { \guitarDynamics }
    \new Staff << \new Voice { \guitar } >>
  >>

  \header {
    piece = \markup \fontsize #3 "1. The Vagabond"
  }
  \layout {
    \context {
      \Staff \RemoveEmptyStaves
    }
  }
}

\pageBreak

\include "2 Let Beauty Awake/common.ly"
\include "2 Let Beauty Awake/music/voice.ly"
\include "2 Let Beauty Awake/lyrics.ly"
\include "2 Let Beauty Awake/music/guitar.ly"

\score {
  <<
    \new Staff << \new Voice = "voice" { \voiceMusic } >>
    \new Lyrics \voiceLyrics
    \new Dynamics { \guitarDynamics }
    \new Staff << \new Voice { \guitar } >>
  >>

  \header {
    piece = \markup \fontsize #3 "2. Let Beauty awake"
  }
  \layout {
    \context {
      \Staff \RemoveEmptyStaves
    }
  }
}
