<a href="https://liberapay.com/contrapunctus/donate"><img alt="Donate using Liberapay" src="https://img.shields.io/liberapay/receives/contrapunctus.svg?logo=liberapay"></a>

# Songs of Travel
Ralph Vaughan Williams' song cycle, arranged for medium voice and solo guitar.

## PDF
The PDF is available [here](https://tilde.team/~contrapunctus/music-scores.html).

## Building
Run the [`mkly`](https://tildegit.org/contrapunctus/mkly) build script -
```sh
./mkly main.ly   # PDF without point-and-click
./mkly dev       # PDF with point-and-click
```

## License
This arrangement is released into the public domain using the [CC0](COPYING).

## Contact
Ways to contact me are listed [here](https://tilde.team/~contrapunctus/contact.html).
